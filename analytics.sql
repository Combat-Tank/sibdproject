-- ANALYTIC QUERY 1
SELECT EXTRACT(YEAR FROM trip_info.trip_start_date) AS yr,
	EXTRACT(MONTH FROM trip_info.trip_start_date) AS mon,
	trip_info.trip_start_date AS dt,
	COUNT(*) AS trip_number
FROM trip_info
GROUP BY GROUPING SETS ((yr),(mon),(dt));

-- ANALYTIC QUERY 2

SELECT country_name_origin AS country_name,
       loc_name_origin AS loc_origin,
       COUNT (*) AS trip_number
FROM trip_info
GROUP BY ROLLUP (country_name, loc_origin);