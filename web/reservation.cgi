#!/usr/bin/python3
import psycopg2
import login
print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')

print('<h3>List of all reservations:</h3>')

connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT * FROM reservation;'
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)


  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>CNI</h3></td>')
  print('<td><h3>Boat ISO</h3></td>')
  print('<td><h3>Sailor ID</h3></td>')
  print('<td><h3>Sailor ISO</h3></td>')
  print('<td><h3>Start Date</h3></td>')
  print('<td><h3>End Date</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('<td><a href="remove_reservation.cgi?cni={}&boat_iso={}&sailor_id={}&sailor_iso={}&start_date={}&end_date={}">Remove Reservation</a></td>'.format(row[0],row[1],row[2],row[3],row[4],row[5]))
    print('</tr>')
  print('</table>')
  print('<form action="add_reservation.cgi" method="post">')
  print('<p><input type="submit" value="Add Reservation"/></p>')


  #Closing connection
  cursor.close()


except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('</body>')
print('</html>')
