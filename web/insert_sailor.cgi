#!/usr/bin/python3
import psycopg2, cgi
import login

form = cgi.FieldStorage()
#getvalue uses the names from the form in previous page
id = form.getvalue('id')
iso_code = form.getvalue('iso_code')
print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')


connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()

  # Making insert
  sql = """
          INSERT INTO sailor
          VALUES(%s, %s);

        """
  data = (id, iso_code)
  
  message = 'New Sailor added has Sailor with id %s and iso code %s'
  
  cursor.execute(sql, data)
  # Commit the update 
  connection.commit()
  print('<h3>{}</h3>'.format(message % data))
  # Closing connection
  cursor.close()
except Exception as e:
  # Print errors on the webpage if they occur
  print('<h1>Insert not valid</h1>')
  print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
      connection.close()
print('</body>')
print('</html>')
