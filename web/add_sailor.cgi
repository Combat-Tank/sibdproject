#!/usr/bin/python3
import cgi
import psycopg2
import login

form = cgi.FieldStorage()
cni = form.getvalue('cni')
iso_code = form.getvalue('iso_code')

print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')


print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')


connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT p.name,p.id,p.iso_code FROM person p LEFT JOIN sailor s ON p.id = s.id AND p.iso_code = s.iso_code WHERE s.id IS NULL AND s.iso_code IS NULL;'
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  print('<h3>This are all the persons that are not sailors in the database</h3>')
  
  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>Name</h3></td>')
  print('<td><h3>ID</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')

except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('<h3>Add new person as owners</h3>')

# The form will send the info needed for the SQL insertion
print('<form action="insert_sailor.cgi" method="post">')
print('<p><input type="hidden" name="Boat" value="{}"/></p>')
print('<p>Sailor id: <input type="text" name="id"/></p>')
print('<p>Sailor iso: <input type="text" name="iso_code"/> </p>')
print('<p><input type="submit" value="Submit"/></p>')
print('</form>')


print('</body>')
print('</html>')
