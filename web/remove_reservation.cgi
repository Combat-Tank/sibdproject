#!/usr/bin/python3
import psycopg2, cgi
import login

form = cgi.FieldStorage()
#getvalue uses the names from the form in previous page
cni = form.getvalue('cni')
boat_iso = form.getvalue('boat_iso')
sailor_id = form.getvalue('sailor_id')
sailor_iso = form.getvalue('sailor_iso')
start_date = form.getvalue('start_date')
end_date = form.getvalue('end_date')


print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')

connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()

  sql = "START TRANSACTION"
  cursor.execute(sql)

  # Making delete
  sql = """
          DELETE
          FROM trip t
          WHERE t.cni = %s AND t.iso_code_boat = %s AND t.id_sailor = %s AND t.iso_code_sailor = %s AND t.start_date = %s AND t.end_date = %s;
        """
  data = (cni, boat_iso, sailor_id, sailor_iso, start_date, end_date)
  
  cursor.execute(sql, data)

  # Making delete
  sql = """
          DELETE
          FROM reservation r
          WHERE r.cni = %s AND r.iso_code_boat = %s AND r.id_sailor = %s AND r.iso_code_sailor = %s AND r.start_date = %s AND r.end_date = %s;
        """
  data = (cni, boat_iso, sailor_id, sailor_iso, start_date, end_date)
  
  cursor.execute(sql, data)
  # Commit the update
  connection.commit()
  message = 'trips of reservation of boat with cni %s iso code %s and sailor id %s and iso %s starting in %s and ending in %s removed from database'
  print('<h3>{}</h3>'.format(message % data))
  message = 'reservation of boat with cni %s iso code %s and sailor id %s and iso %s starting in %s and ending in %s removed from database'
  print('<h3>{}</h3>'.format(message % data))
  # Closing connection
  cursor.close()
except Exception as e:
  # Print errors on the webpage if they occur
  print('<h1>Please remove all the dependent data this reservation has before removing it</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
      connection.close()
print('</body>')
print('</html>')
