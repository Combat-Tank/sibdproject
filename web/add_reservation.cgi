#!/usr/bin/python3
import cgi
import psycopg2
import login

form = cgi.FieldStorage()

print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')

connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT * FROM boat;'
  print('<h3>This are all the boats in the database</h3>')
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>Name</h3></td>')
  print('<td><h3>year</h3></td>')
  print('<td><h3>CNI</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('<td><h3>Owner ID</h3></td>')
  print('<td><h3>Owner ISO</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')

  # Making query
  sql = 'SELECT * FROM sailor NATURAL JOIN person;'
  print('<h3>This are all the sailors in the database</h3>')
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>id</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('<td><h3>Name</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')

   # Making query
  sql = 'SELECT * FROM schedule;'
  print('<h3>This are all the schedules in the database</h3>')
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>Start Date</h3></td>')
  print('<td><h3>End Date</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')

except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('<h3>Add new person as owners</h3>')

# The form will send the info needed for the SQL insertion
print('<form action="insert_reservation.cgi" method="post">')
print('<p><input type="hidden" name="Reservation" value="{}"/></p>')
print('<p>Reservation Boat CNI: <input type="text" name="cni"/> </p>')
print('<p>Reservation Boat ISO Code: <input type="text" name="iso_code"/> </p>')
print('<p>Reservation Sailor id: <input type="text" name="sailor_id"/></p>')
print('<p>Reservation Sailor ISO: <input type="text" name="sailor_iso"/></p>')
print('<p>Reservation Start date: <input type="date" name="start_date"/></p>')
print('<p>Reservation End date: <input type="date" name="end_date"/></p>')
print('<p><input type="submit" value="Submit"/></p>')
print('</form>')


print('</body>')
print('</html>')
