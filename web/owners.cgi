#!/usr/bin/python3
import psycopg2

import login


print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')

print('<h3>List of all Owners:</h3>')


connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT * FROM owner NATURAL JOIN person;'
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)


  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>ID</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('<td><h3>Birthday</h3></td>')
  print('<td><h3>Name</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('<td><a href="remove_owner.cgi?owner_id={}&owner_iso_code={}">Remove Owner</a></td>'.format(row[0],row[1]))
    print('</tr>')
  print('</table>')
  print('<form action="add_owner.cgi" method="post">')
  print('<p><input type="submit" value="Add Owner"/></p>')


  #Closing connection
  cursor.close()


except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('</body>')
print('</html>')
