#!/usr/bin/python3
import cgi
import psycopg2
import login

form = cgi.FieldStorage()

print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')


print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')


connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT name,iso_code FROM country;'
  print('<h3>This are all the country names in the database</h3>')
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>Name</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')

  # Making query
  sql = 'SELECT p.id,p.iso_code,p.name FROM person p LEFT JOIN owner o ON p.id = o.id AND p.iso_code = o.iso_code WHERE o.id IS NOT NULL AND o.iso_code IS NOT NULL;'
  print('<h3>This are all the owners in the database</h3>')
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)

  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>ID</h3></td>')
  print('<td><h3>ISO</h3></td>')
  print('<td><h3>Name</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('</tr>')
  print('</table>')
  

except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('<h3>Add new person as owners</h3>')

# The form will send the info needed for the SQL insertion
print('<form action="insert_boat.cgi" method="post">')
print('<p><input type="hidden" name="Boat" value="{}"/></p>')
print('<p>Boat Name: <input type="text" name="name"/></p>')
print('<p>Boat CNI: <input type="text" name="cni"/> </p>')
print('<p>Boat ISO Code: <input type="text" name="iso_code"/> </p>')
print('<p>Boat year of registration: <input type="number" name="year"/></p>')
print('<p>Owner ISO Code: <input type="text" name="owner_iso_code"/> </p>')
print('<p>Owner id: <input type="number" name="owner_id"/> </p>')
print('<p><input type="submit" value="Submit"/></p>')
print('</form>')


print('</body>')
print('</html>')
