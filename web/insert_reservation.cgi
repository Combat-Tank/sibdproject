#!/usr/bin/python3
import psycopg2, cgi
import login

form = cgi.FieldStorage()
#getvalue uses the names from the form in previous page
name = form.getvalue('name')
cni = form.getvalue('cni')
iso_code = form.getvalue('iso_code')
sailor_id = form.getvalue('sailor_id')
sailor_iso = form.getvalue('sailor_iso')
start_date = form.getvalue('start_date')
end_date = form.getvalue('end_date')
print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')


connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()

  # Making insert
  sql = """
          INSERT INTO reservation
          VALUES(%s, %s, %s, %s, %s, %s);

        """
  data = (cni, iso_code, sailor_id, sailor_iso, start_date, end_date)
  
  message = 'New Reservation added has Reservation with cni %s boat iso %s sailor id %s sailor iso %s start date %s and end date %s'
  
  cursor.execute(sql, data)
  # Commit the update
  connection.commit()
  print('<h3>{}</h3>'.format(message % data))
  # Closing connection
  cursor.close()
except Exception as e:
  # Print errors on the webpage if they occur
  print('<h1>Insert not valid</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
      connection.close()
print('</body>')
print('</html>')
