#!/usr/bin/python3
import psycopg2, cgi
import login
form = cgi.FieldStorage()
print('Content-type:text/html\n\n')
print('<html>')
print('<head>')
print('<title>Project Part 3</title>')
print('</head>')
print('<body>')

print('<h1>MENU</h1>')

print('<tr>')
print('<td><a href="owners.cgi">Owners</a></td>')
print('<td><a href="boat.cgi">Boats</a></td>')
print('<td><a href="list_sailor.cgi">Sailors</a></td>')
print('<td><a href="reservation.cgi">Reservations</a></td>')
print('</tr>')

print('<h3>List of all Sailors:</h3>')

connection = None
try:
  # Creating connection
  connection = psycopg2.connect(login.credentials)
  cursor = connection.cursor()


  # Making query
  sql = 'SELECT p.name,p.id,p.iso_code FROM sailor s JOIN person p ON s.id = p.id AND s.iso_code = p.iso_code;'
  cursor.execute(sql)
  result = cursor.fetchall()
  num = len(result)


  # Displaying results
  print('<table border="3" cellspacing="5">')
  print('<tr>')
  print('<td><h3>Sailor Name</h3></td>')
  print('<td><h3>Sailor id</h3></td>')
  print('<td><h3>Sailor ISO</h3></td>')
  print('</tr>')
  for row in result:
    print('<tr>')
    for value in row:
      print('<td>{}</td>'.format(value))
    print('<td><a href="remove_sailor.cgi?id={}&iso_code={}">Remove Sailor</a></td>'.format(row[1],row[2]))
    print('</tr>')
  print('</table>')
  print('<form action="add_sailor.cgi" method="post">')
  print('<p><input type="submit" value="Add Sailor"/></p>')


  #Closing connection
  cursor.close()


except Exception as e:
  print('<h1>An error occurred.</h1>')
  ##print('<p>{}</p>'.format(e))
finally:
  if connection is not None:
    connection.close()

print('</body>')
print('</html>')
