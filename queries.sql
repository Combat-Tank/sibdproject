--- QUERY 1
SELECT iso, id, name, COUNT((a.cni, a.boat_iso)) AS nr_boats
FROM (
    SELECT owner.id as id, owner.iso_code as iso, person.name as name, boat.iso_code as boat_iso, boat.cni
    FROM boat
        INNER JOIN owner
        ON (boat.id_owner, boat.iso_code_owner) = (owner.id, owner.iso_code)
        INNER JOIN person
        ON (person.id, person.iso_code) = (owner.id, owner.iso_code)
    ) AS a
GROUP BY iso, name, id
HAVING (iso, COUNT((a.cni, a.boat_iso))) IN(
    SELECT iso, MAX(nr_boats)
    FROM (
        SELECT iso, id, name, COUNT(id) AS nr_boats
        FROM (
        SELECT owner.id as id, owner.iso_code as iso, person.name as name FROM boat
            INNER JOIN owner
            ON (boat.id_owner, boat.iso_code_owner) = (owner.id, owner.iso_code)
            INNER JOIN person
            ON (person.id, person.iso_code) = (owner.id, owner.iso_code)
        ) AS c
        GROUP BY iso, id, name
    ) AS d
    GROUP BY iso
);

--- QUERY 2
SELECT name, id, owner_iso
FROM (
    SELECT boat_iso, name, owner_iso, id, COUNT((id, owner_iso)) AS nr_boats
    FROM (
        SELECT owner.id as id, owner.iso_code AS owner_iso, boat.iso_code AS boat_iso, person.name AS name
        FROM boat
            LEFT OUTER JOIN owner
            ON (boat.id_owner, boat.iso_code_owner) = (owner.id, owner.iso_code)
            INNER JOIN person
            ON (person.id, person.iso_code) = (owner.id, owner.iso_code)
        ) AS a
    GROUP BY boat_iso, name, owner_iso, id
    HAVING COUNT((id, owner_iso)) >= 2
    )AS b
GROUP BY name, id, owner_iso
HAVING COUNT(boat_iso) >= 2;

--- QUERY 3
SELECT sailor.id AS idA, sailor.iso_code AS isoA, p.name AS nameA
FROM sailor
    INNER JOIN person p
    ON sailor.id = p.id AND sailor.iso_code = p.iso_code
    INNER JOIN trip
          ON trip.id_sailor = sailor.id AND trip.iso_code_sailor = sailor.iso_code
    WHERE (end_latitude, end_longitude) IN (
        SELECT location.latitude, location.longitude--, location.name as name
        FROM location
            INNER JOIN country
            ON location.iso_code = country.iso_code
        WHERE country.name = 'Portugal'
    )
GROUP BY sailor.id, sailor.iso_code, p.name
HAVING COUNT(DISTINCT (end_latitude, end_longitude)) IN (
        SELECT COUNT(*)
        FROM (
            SELECT location.latitude, location.longitude, location.name as name
            FROM location
            INNER JOIN country
            ON location.iso_code = country.iso_code
            WHERE country.name = 'Portugal'
        ) AS b
);

--- QUERY 4
SELECT sailor.iso_code, sailor.id , p.name, r.cni, r.iso_code_boat, r.start_date, r.end_date
FROM sailor
    INNER JOIN person p
    ON p.id = sailor.id AND p.iso_code = sailor.iso_code
    INNER JOIN trip
          ON sailor.id = trip.id_sailor AND sailor.iso_code = trip.iso_code_sailor
    INNER JOIN reservation r
          ON sailor.id = r.id_sailor AND sailor.iso_code = r.iso_code_sailor AND trip.cni = r.cni AND
             trip.iso_code_boat = r.iso_code_boat AND trip.id_sailor = r.id_sailor AND
             trip.iso_code_sailor = r.iso_code_sailor AND trip.start_date = r.start_date AND trip.end_date = r.end_date
GROUP BY sailor.iso_code, sailor.id , p.name, r.cni, r.iso_code_boat, r.start_date, r.end_date
HAVING (sailor.id, sailor.iso_code) IN(
    SELECT trip.id_sailor, trip.iso_code_sailor
    FROM trip
    GROUP BY trip.id_sailor, trip.iso_code_sailor
    HAVING COUNT(*) >= ALL(
        SELECT COUNT(*)
        FROM trip
        GROUP BY trip.id_sailor, trip.iso_code_sailor
        )
);

--- QUERY 5
SELECT sailor.iso_code, sailor.id ,p.name, r.cni, r.iso_code_boat, r.start_date, r.end_date, SUM(trip.duration) AS time_travelling
FROM sailor
    INNER JOIN person p
    ON p.id = sailor.id AND p.iso_code = sailor.iso_code
    INNER JOIN trip
          ON sailor.id = trip.id_sailor AND sailor.iso_code = trip.iso_code_sailor
    INNER JOIN reservation r
          ON sailor.id = r.id_sailor AND sailor.iso_code = r.iso_code_sailor AND trip.cni = r.cni AND
             trip.iso_code_boat = r.iso_code_boat AND trip.id_sailor = r.id_sailor AND
             trip.iso_code_sailor = r.iso_code_sailor AND trip.start_date = r.start_date AND trip.end_date = r.end_date
GROUP BY sailor.iso_code, sailor.id ,p.name, r.cni, r.iso_code_boat, r.start_date, r.end_date
HAVING SUM(trip.duration) >= ALL(
    SELECT SUM(trip.duration)
    FROM sailor
        INNER JOIN person p
        ON p.id = sailor.id AND p.iso_code = sailor.iso_code
        INNER JOIN trip
              ON sailor.id = trip.id_sailor AND sailor.iso_code = trip.iso_code_sailor
        INNER JOIN reservation r
              ON sailor.id = r.id_sailor AND sailor.iso_code = r.iso_code_sailor AND trip.cni = r.cni AND
                 trip.iso_code_boat = r.iso_code_boat AND trip.id_sailor = r.id_sailor AND
                 trip.iso_code_sailor = r.iso_code_sailor AND trip.start_date = r.start_date AND trip.end_date = r.end_date
    GROUP BY sailor.iso_code, sailor.id ,p.name, trip.cni, trip.iso_code_boat, trip.start_date, trip.end_date
)
