# SIBDProject Project Assignment

## Description
Repository dedicated to the project development of the Information Systems and Databases subject at Instituto Superior Técnico.

## Part 3 - Development of SQL queries, integrity constraints and creation of a Web App.
This projects aims at creating a web app servicing a database, with various custom queries and an indexing system.

### Doubts
1. Should we only access information on the database for the website through views?

Not necessary

2. Should the add a owner web app be able to add a person too?

We can do the two of them, but one is enough 

3. Should the removal of a owner remove a boat as well or give a treated exception or can we just print the exception as an error?

Just remove all things that depend on the owner existence (Boat) with everything on the action as a transaction. (use on delete cascade)

4. Should we treat properly the error messagens in the web apps?
 
For error messages print something diferent more appeling to the user not just the error string.
