START TRANSACTION;
SET CONSTRAINTS ALL DEFERRED;

-- countries
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/255px-Flag_of_Portugal.svg.png', 'Portugal', 'PR');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/a/af/Flag_of_South_Africa.svg', 'South Africa', 'ZF');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/9/93/Flag_of_the_Bahamas.svg','Bahamas',  'BS');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/3/38/Gibraltar_flag_large.png', 'Gibraltar', 'GB');
INSERT INTO country VALUES('https://laendercode.net/img/flag-icon-css/flags/4x3/ky.svg', 'Cayman Islands', 'CM');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/0/0a/Flag_of_Jamaica.svg','Jamaica', 'JM');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/4/42/Flag_of_the_British_Virgin_Islands.svg', 'British Virgin Islands', 'VG');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Flag_of_Liberia.svg/1200px-Flag_of_Liberia.svg.png', 'Liberia',  'LB');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/2/2e/Flag_of_the_Marshall_Islands.svg', 'Marshall Islands','ML' );
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_Finland.svg','Finland', 'FN');
INSERT INTO country VALUES('https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/2560px-Flag_of_Italy.svg.png', 'Italy', 'IA');

-- location
INSERT INTO "location" VALUES('Marina de Vilamoura', 37.0868, -8.1244, 'PR');
INSERT INTO "location" VALUES('Port of Cape Town', -33.918861, 18.423300, 'ZF');
INSERT INTO "location" VALUES('Cais do Sodré', 38.7062, -9.1450, 'PR');
INSERT INTO "location" VALUES('Vicci Gaming', 40.7062, -56.1450, 'IA');
INSERT INTO "location" VALUES('Kingston', 50.7062, -56.1450, 'JM');
INSERT INTO "location" VALUES('Vicci Gaming', 45.7062, -56.1450, 'VG');

INSERT INTO marina VALUES(37.0868, -8.1244);
INSERT INTO marina VALUES(38.7062, -9.1450);

INSERT INTO port VALUES(-33.918861, 18.423300);
INSERT INTO port VALUES(50.7062, -56.1450);

INSERT INTO wharf VALUES(40.7062, -56.1450);
INSERT INTO wharf VALUES(45.7062, -56.1450);

-- person
INSERT INTO person VALUES(1, 'John F. Kennedy', 'CM');
INSERT INTO person VALUES(2, 'Marshall Mathers', 'ML');
INSERT INTO person VALUES(3, 'Kimi Raikkonen', 'FN');
INSERT INTO person VALUES(4, 'Tiago Pinto', 'PR');
INSERT INTO person VALUES(5, 'Joao Rendeiro', 'ZF');
INSERT INTO person VALUES(6, 'Miguel Arrendeiro', 'ZF');
INSERT INTO person VALUES(7, 'Toy', 'ZF');
INSERT INTO person VALUES(8, 'Mario Cotrim', 'VG');
INSERT INTO person VALUES(9, 'Manas2000', 'PR');

-- owner
INSERT INTO "owner" VALUES(4, 'PR', '03-06-2000');
INSERT INTO "owner" VALUES(5, 'ZF','22-05-1952');
INSERT INTO "owner" VALUES(6, 'ZF', '12-08-1999');
INSERT INTO "owner" VALUES(7, 'ZF', '12-08-1999');
INSERT INTO "owner" VALUES(8, 'VG', '12-10-2000');
INSERT INTO "owner" VALUES(9, 'PR','30-08-2000');

-- sailor
INSERT INTO sailor VALUES(1, 'CM');
INSERT INTO sailor VALUES(2, 'ML');
INSERT INTO sailor VALUES(8, 'VG');
INSERT INTO sailor VALUES(4, 'PR');

-- boat
INSERT INTO boat VALUES('Sunny Go', 2010, 1, 'JM', 4, 'PR');
INSERT INTO boat VALUES('Sistema', 2019, 2, 'VG', 8, 'VG');
INSERT INTO boat VALUES('Sistema2', 2019, 3, 'IA', 8, 'VG');
INSERT INTO boat VALUES('Going Merry', 2000, 4, 'ZF', 9, 'PR');
INSERT INTO boat VALUES('Arca de Noe', 2000, 5, 'PR', 9, 'PR');
INSERT INTO boat VALUES('Impel Down', 2005, 6, 'ZF', 5, 'ZF');
INSERT INTO boat VALUES('Mega Barco', 1905, 7, 'ZF', 6, 'ZF');
INSERT INTO boat VALUES('Oro Jackson', 1905, 8, 'ZF', 4, 'PR');
INSERT INTO boat VALUES('Barco a Remo', 1500, 9, 'ZF', 6, 'ZF');
INSERT INTO boat VALUES('Flying Dutchman', 1550, 10, 'JM', 4, 'PR');
INSERT INTO boat VALUES('Mobydick', 1999, 11, 'ZF', 4, 'PR');

-- boat_with_vhf
INSERT INTO boat_vhf VALUES(193187, 1, 'JM');
INSERT INTO boat_vhf VALUES(193123, 4, 'ZF');

-- schedule
INSERT INTO schedule VALUES('03-06-2021', '05-06-2021');
INSERT INTO schedule VALUES('07-06-2021', '08-06-2021');
INSERT INTO schedule VALUES('08-06-2021', '10-07-2021');
INSERT INTO schedule VALUES('03-07-2021', '05-07-2021');
INSERT INTO schedule VALUES('03-07-2023', '30-07-2023');

-- reservation
INSERT INTO reservation VALUES(2, 'VG', 1, 'CM', '03-06-2021', '05-06-2021');
INSERT INTO reservation VALUES(3, 'IA', 1, 'CM', '07-06-2021', '08-06-2021');
INSERT INTO reservation VALUES(1, 'JM', 1, 'CM', '08-06-2021', '10-07-2021');
INSERT INTO reservation VALUES(5, 'PR', 1, 'CM', '03-07-2021', '05-07-2021');
INSERT INTO reservation VALUES(5, 'PR', 1, 'CM', '03-07-2023', '30-07-2023');


-- trip
INSERT INTO trip VALUES('03-06-2021','1', 2, 'VG', 1, 'CM', '03-06-2021', '05-06-2021', 38.7062, -9.1450, 37.0868, -8.1244);
INSERT INTO trip VALUES('07-06-2021', '1', 3, 'IA', 1, 'CM', '07-06-2021', '08-06-2021', 37.0868, -8.1244, 38.7062, -9.1450);
INSERT INTO trip VALUES('09-06-2021', '1', 1, 'JM', 1, 'CM', '08-06-2021', '10-07-2021', -33.918861, 18.423300, 38.7062, -9.1450);
INSERT INTO trip VALUES('04-07-2023', 10, 5, 'PR', 1, 'CM', '03-07-2023', '30-07-2023' , -33.918861, 18.423300, 38.7062, -9.1450);
INSERT INTO trip VALUES('16-07-2023', 4, 5, 'PR', 1, 'CM', '03-07-2023', '30-07-2023' , -33.918861, 18.423300, 38.7062, -9.1450);

COMMIT;



