-- IC-1 check time overlap
CREATE OR REPLACE FUNCTION chk_date_interval_proc()
RETURNS TRIGGER AS
$$
    DECLARE cursor_trips CURSOR FOR
        SELECT cni, iso_code_boat, date, duration FROM trip;
    DECLARE boat_cni VARCHAR(15);
    DECLARE boat_ISO CHAR(2);
    DECLARE start_d DATE;
    DECLARE duration SMALLINT;
    DECLARE end_d DATE;
BEGIN
    OPEN cursor_trips;
    LOOP
        FETCH cursor_trips INTO boat_cni, boat_iso, start_d, duration;
        end_d = start_d + duration;
        EXIT WHEN NOT FOUND;
        IF boat_cni = NEW.cni AND boat_iso = NEW.iso_code_boat THEN
            IF end_d >= NEW.date AND start_d <= (NEW.date + NEW.duration) THEN
                RAISE EXCEPTION 'Date Overlaps'
                USING HINT = 'Please choose a new date';
            END IF;
        END IF;
    END LOOP;
    CLOSE cursor_trips;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER  IF EXISTS chk_date_interval
    ON trip;

CREATE TRIGGER chk_date_interval
BEFORE UPDATE OR INSERT ON trip
FOR EACH ROW EXECUTE PROCEDURE chk_date_interval_proc();

--------------- IC2

CREATE OR REPLACE FUNCTION chk_location_specification()
RETURNS TRIGGER AS

$$
DECLARE presence SMALLINT DEFAULT 0;

BEGIN
    IF (NEW.latitude, NEW.longitude) IN (SELECT wharf.latitude, wharf.longitude FROM wharf) THEN
        presence = presence + 1;
    END IF;
    IF (NEW.latitude, NEW.longitude) IN (SELECT port.latitude, port.longitude FROM port) THEN
        presence = presence + 1;
    END IF;
    IF (NEW.latitude, NEW.longitude) IN (SELECT marina.latitude, marina.longitude FROM marina) THEN
        presence = presence + 1;
    END IF;
    IF presence <> 1 THEN
        RAISE EXCEPTION 'Location % must be only one of this types: port, location or marina', NEW.name;
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;
CREATE CONSTRAINT TRIGGER chk_location_specification
AFTER UPDATE OR INSERT ON location DEFERRABLE
FOR EACH ROW EXECUTE PROCEDURE chk_location_specification();

--------------- IC3
CREATE OR REPLACE FUNCTION chk_boat_location()
RETURNS TRIGGER AS

$$
BEGIN
    IF NEW.iso_code NOT IN (SELECT location.iso_code FROM location) THEN
        RAISE EXCEPTION 'No location available in country %', NEW.iso_code;
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS  chk_boat_location
    ON boat;

CREATE TRIGGER chk_boat_location
BEFORE UPDATE OR INSERT ON boat
FOR EACH ROW EXECUTE PROCEDURE chk_boat_location();