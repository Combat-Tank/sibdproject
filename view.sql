CREATE OR REPLACE VIEW trip_info(
    country_iso_origin, country_name_origin,
    country_iso_dest, country_name_dest,
    loc_name_origin,
    loc_name_dest,
    cni_boat,
    country_iso_boat, country_name_boat,
    trip_start_date)
AS
    SELECT origin.iso_code AS country_iso_origin,
        origin.name AS country_name_origin,
        dest.iso_code AS country_iso_dest,
        dest.name AS country_name_dest,
        loc_origin.name AS loc_name_origin,
        loc_dest.name AS loc_name_dest,
        trip_boat.cni AS cni_boat,
        trip_boat.iso_code AS country_iso_boat,
        trip_boat_country.name AS country_name_boat,
        trip.start_date AS trip_start_date
    FROM trip
    INNER JOIN
        location loc_origin 
        ON (trip.start_latitude, trip.start_longitude) = (loc_origin.latitude, loc_origin.longitude)
    INNER JOIN
        location loc_dest
        ON (trip.end_latitude, trip.end_longitude) = (loc_dest.latitude, loc_dest.longitude)
    INNER JOIN
        country origin
        ON origin.iso_code = loc_origin.iso_code
    INNER JOIN 
        country dest
        ON dest.iso_code = loc_dest.iso_code
    INNER JOIN
        boat trip_boat
        ON (trip.cni, trip.iso_code_boat) = (trip_boat.cni, trip_boat.iso_code)
    INNER JOIN
        country trip_boat_country
        ON trip_boat_country.iso_code = trip_boat.iso_code;

SELECT * FROM trip_info;