# SIBDProject Project Assignment

## Description
Repository dedicated to the project development of the Information Systems and Databases subject at Instituto Superior Técnico.

## Part 2 - Schema Creation and Data Insertion
This projects aims at creating, populating and querying a database from an Entity-Association model.

### Doubts
1. Should we add constraints in table creation or do an ALTER TABLE Statement afterward?
(esta como não percebo a 100% não vou perguntar)

2. Should longitude, latitude pair in Location table be one point variable or any other datatype different from 2 float types?
https://stackoverflow.com/questions/8150721/which-data-type-for-latitude-and-longitude
https://docs.microsoft.com/en-us/sql/t-sql/spatial-geography/point-geography-data-type?view=sql-server-ver15
(É para nos mantermos nos standards basicos de SQL não ir para cenas estranhas, portanto manter longitude e latitude separados)

3. Flag image will be a VARBINARY type similar to how an emoji works, like a base 64 variable
https://stackoverflow.com/questions/5613898/storing-images-in-sql-server
https://stackoverflow.com/questions/54500/storing-images-in-postgresql/10267699
(pode ser binario ou um url para a imagem)

4. Should we attribute ID CARD and ISO CODE with our own hash or just use SERIAL datatype?
For now i will assume ID and ISO are values smaller than +2147483647.
(desde que faça sentido o prof é na boa com o que escolhermos, convem funcionar para pelo menos um standard caso haja varios)

5. os nomes das tabelas têm de ser em minusculas por notação?
(Basicamente devemos usar sempre minusculas, e usar em geral a notação que o prof usa nos slides para tudo tanto colunas e tabelas)
