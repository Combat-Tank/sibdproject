--Query A

cni,iso_code,owner_id_card,owner_iso_code,name,year,length
1,JAM,4,POR,Sunny Go,2010,50
2,VG,8,VG,Sistema,2019,4
3,ITA,8,VG,Sistema2,2019,4
5,POR,9,POR,Arca de Noe,2000,13

--Query B

name,id_card,iso_code
John F. Kennedy,1,CYM

--Query C

sched_start_date,sched_end_date,sailor_id_card,sailor_iso_code,boat_cni,boat_iso_code
2021-06-08,2021-07-10,1,CYM,1,JAM

--Query D

name,cni
Impel Down,6