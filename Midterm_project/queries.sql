-- Query A 
SELECT * 
FROM boat 
WHERE (iso_code, cni) IN (
    SELECT boat_iso_code, boat_cni 
    FROM reservation);

-- Query B
SELECT person.name, sailor.id_card, sailor.iso_code
FROM sailor INNER JOIN person 
    ON (sailor.id_card = person.id_card AND sailor.iso_code = person.iso_code) 
WHERE (sailor.iso_code, sailor.id_card) IN (
    SELECT sailor_iso_code, sailor_id_card 
    FROM reservation 
    WHERE boat_iso_code IN (
        SELECT iso_code 
        FROM country 
        WHERE ("name" = 'Portugal')));


-- Query C
SELECT * 
FROM reservation
WHERE sched_end_date - sched_start_date > 5;

-- Query D
SELECT boat."name", boat.cni
FROM boat INNER JOIN country 
    ON (boat.iso_code = country.iso_code)
WHERE (country.name = 'South Africa')
AND (boat.owner_id_card, boat.owner_iso_code) IN (
    SELECT id_card,iso_code 
    FROM person 
    WHERE "name" LIKE '%Rendeiro');