-- countries
INSERT INTO country VALUES('POR', 'Portugal', 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/255px-Flag_of_Portugal.svg.png');
INSERT INTO country VALUES('ZAF', 'South Africa', 'https://upload.wikimedia.org/wikipedia/commons/a/af/Flag_of_South_Africa.svg');
INSERT INTO country VALUES('BHS', 'Bahamas', 'https://upload.wikimedia.org/wikipedia/commons/9/93/Flag_of_the_Bahamas.svg');
INSERT INTO country VALUES('GIB', 'Gibraltar', 'https://upload.wikimedia.org/wikipedia/commons/3/38/Gibraltar_flag_large.png');
INSERT INTO country VALUES('CYM', 'Cayman Islands', 'https://laendercode.net/img/flag-icon-css/flags/4x3/ky.svg');
INSERT INTO country VALUES('JAM', 'Jamaica', 'https://upload.wikimedia.org/wikipedia/commons/0/0a/Flag_of_Jamaica.svg');
INSERT INTO country VALUES('VG', 'British Virgin Islands', 'https://upload.wikimedia.org/wikipedia/commons/4/42/Flag_of_the_British_Virgin_Islands.svg');
INSERT INTO country VALUES('LIB', 'Liberia', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Flag_of_Liberia.svg/1200px-Flag_of_Liberia.svg.png');
INSERT INTO country VALUES('MHL', 'Marshall Islands', 'https://upload.wikimedia.org/wikipedia/commons/2/2e/Flag_of_the_Marshall_Islands.svg');
INSERT INTO country VALUES('FIN', 'Finland', 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_Finland.svg');
INSERT INTO country VALUES('ITA', 'Italy', 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/2560px-Flag_of_Italy.svg.png');


-- person
INSERT INTO person VALUES(1, 'CYM', 'John F. Kennedy');
INSERT INTO person VALUES(2, 'MHL', 'Marshall Mathers');
INSERT INTO person VALUES(3, 'FIN', 'Kimi Raikkonen');
INSERT INTO person VALUES(4, 'POR', 'Tiago Pinto');
INSERT INTO person VALUES(5, 'ZAF', 'Joao Rendeiro');
INSERT INTO person VALUES(6, 'ZAF', 'Miguel Arrendeiro');
INSERT INTO person VALUES(7, 'ZAF', 'Toy');
INSERT INTO person VALUES(8, 'VG', 'Mario Cotrim');
INSERT INTO person VALUES(9, 'POR', 'Manas2000');

-- owner
INSERT INTO "owner" VALUES(4, 'POR', '03-06-2000');
INSERT INTO "owner" VALUES(5, 'ZAF','22-05-1952');
INSERT INTO "owner" VALUES(6, 'ZAF', '12-08-1999');
INSERT INTO "owner" VALUES(7, 'ZAF', '12-08-1999');
INSERT INTO "owner" VALUES(8, 'VG', '12-10-2000');
INSERT INTO "owner" VALUES(9, 'POR','30-08-2000');

-- sailor
INSERT INTO sailor VALUES(1, 'CYM');
INSERT INTO sailor VALUES(2, 'MHL');
INSERT INTO sailor VALUES(8, 'VG');

-- boat
INSERT INTO boat VALUES(1, 'JAM', 4, 'POR', 'Sunny Go', 2010, 50);
INSERT INTO boat VALUES(2, 'VG', 8, 'VG', 'Sistema', 2019, 4);
INSERT INTO boat VALUES(3, 'ITA', 8, 'VG', 'Sistema2', 2019, 4);
INSERT INTO boat VALUES(4, 'ZAF', 9, 'POR', 'Going Merry', 2000, 13);
INSERT INTO boat VALUES(5, 'POR', 9, 'POR', 'Arca de Noe', 2000, 13);
INSERT INTO boat VALUES(6, 'ZAF', 5, 'ZAF', 'Impel Down', 2005, 20);
INSERT INTO boat VALUES(7, 'ZAF', 6, 'ZAF', 'Mega Barco', 1905, 25);

-- boat_with_vhf
INSERT INTO boat_with_vhf VALUES(1, 'JAM', 193187);
INSERT INTO boat_with_vhf VALUES(4, 'ZAF', 193123);

-- location
INSERT INTO "location" VALUES(37.0868, -8.1244, 'Marina de Vilamoura', 'POR');
INSERT INTO "location" VALUES(-33.918861, 18.423300, 'Port of Cape Town', 'ZAF');
INSERT INTO "location" VALUES(38.7062, -9.1450, 'Cais do Sodré', 'POR');

INSERT INTO marina VALUES(37.0868, -8.1244);

INSERT INTO port VALUES(-33.918861, 18.423300);

INSERT INTO wharf VALUES(38.7062, -9.1450);

-- schedule
INSERT INTO schedule VALUES('03-06-2021', '05-06-2021');
INSERT INTO schedule VALUES('07-06-2021', '08-06-2021');
INSERT INTO schedule VALUES('08-06-2021', '10-07-2021');
INSERT INTO schedule VALUES('03-07-2021', '05-07-2021');

-- reservation
INSERT INTO reservation VALUES('03-06-2021', '05-06-2021', 1, 'CYM', 2, 'VG');
INSERT INTO reservation VALUES('07-06-2021', '08-06-2021', 1, 'CYM', 3, 'ITA');
INSERT INTO reservation VALUES('08-06-2021', '10-07-2021', 1, 'CYM', 1, 'JAM');
INSERT INTO reservation VALUES('03-07-2021', '05-07-2021', 1, 'CYM', 5, 'POR');


-- trip
INSERT INTO trip VALUES('03-06-2021','1 days', '03-06-2021', '05-06-2021', 1, 'CYM', 2, 'VG', 38.7062, -9.1450, 37.0868, -8.1244);
INSERT INTO trip VALUES('07-06-2021', '1 days','07-06-2021', '08-06-2021', 1, 'CYM', 3, 'ITA', 37.0868, -8.1244, 38.7062, -9.1450);
INSERT INTO trip VALUES('09-06-2021', '1 days', '08-06-2021', '10-07-2021', 1, 'CYM', 1, 'JAM', -33.918861, 18.423300, 38.7062, -9.1450);
