DROP TABLE IF EXISTS trip CASCADE;
DROP TABLE IF EXISTS reservation CASCADE;
DROP TABLE IF EXISTS schedule CASCADE;
DROP TABLE IF EXISTS port CASCADE;
DROP TABLE IF EXISTS wharf CASCADE;
DROP TABLE IF EXISTS marina CASCADE;
DROP TABLE IF EXISTS "location" CASCADE;
DROP TABLE IF EXISTS boat_with_vhf CASCADE;
DROP TABLE IF EXISTS boat CASCADE;
DROP TABLE IF EXISTS sailor CASCADE;
DROP TABLE IF EXISTS "owner" CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS country CASCADE;

CREATE TABLE country(
    iso_code VARCHAR(3),
    "name" VARCHAR(80) NOT NULL,
    flag VARCHAR(2083) NOT NULL,
    PRIMARY KEY (iso_code),
    UNIQUE("name"),
    UNIQUE(flag)
);

CREATE TABLE person(
    id_card VARCHAR(18),
    iso_code VARCHAR(3),
    "name" VARCHAR(80) NOT NULL,
    PRIMARY KEY (id_card, iso_code),
    FOREIGN KEY (iso_code)
        REFERENCES Country(iso_code)
    -- Every person must be
    -- in the table 'sailor' or in
    -- the table 'owner'
);

CREATE TABLE "owner"(
    id_card VARCHAR(18),
    iso_code VARCHAR(3),
    birthdate DATE NOT NULL,
    PRIMARY KEY (id_card, iso_code),
    FOREIGN KEY (id_card,iso_code) 
        REFERENCES person(id_card,iso_code)
    -- Every owner must exist
    -- in the table "boat"
);

CREATE TABLE sailor(
    id_card VARCHAR(18),
    iso_code VARCHAR(3),
    PRIMARY KEY (id_card,iso_code),
    FOREIGN KEY (id_card,iso_code) 
        REFERENCES person(id_card,iso_code)
);

CREATE TABLE boat(
    cni VARCHAR(14),
    iso_code VARCHAR(3),
    owner_id_card VARCHAR(18) NOT NULL,
    owner_iso_code VARCHAR(3) NOT NULL,
    "name" VARCHAR(80) NOT NULL,
    year SMALLINT NOT NULL,
    "length" FLOAT NOT NULL,
    PRIMARY KEY (iso_code, cni),
    FOREIGN KEY (iso_code)
        REFERENCES country(iso_code),
    FOREIGN KEY (owner_id_card, owner_iso_code)
        REFERENCES "owner"(id_card, iso_code)
);

CREATE TABLE boat_with_vhf(
    cni VARCHAR(14),
    iso_code VARCHAR(3),
    mmsi NUMERIC(9,0) NOT NULL,
    PRIMARY KEY (cni, iso_code),
    FOREIGN KEY (cni, iso_code) 
        REFERENCES boat(cni, iso_code)
);

CREATE TABLE "location"(
    latitude NUMERIC(8,6),
    longitude NUMERIC(9,6),
    "name" VARCHAR(80) NOT NULL,
    country_iso_code VARCHAR(3) NOT NULL,
    PRIMARY KEY (latitude, longitude),
    FOREIGN KEY (country_iso_code)
        REFERENCES country(iso_code)
    -- Every location must be
    -- in the table 'marina', in the table 'wharf'
    -- or in the table 'port'

    -- No location can exist at the	same
    -- time in the tables 'port', 'marina',
    -- or 'Wharf'

    -- Any two locations must be at
    -- least one mile apart from each other
);

CREATE TABLE marina(
    latitude NUMERIC(8,6),
    longitude NUMERIC(9,6),
    PRIMARY KEY (latitude, longitude),
    FOREIGN KEY (latitude, longitude) 
        REFERENCES "location"(latitude,longitude)
);

CREATE TABLE wharf(
    latitude NUMERIC(8,6),
    longitude NUMERIC(9,6),
    PRIMARY KEY (latitude, longitude),
    FOREIGN KEY (latitude, longitude) 
        REFERENCES "location"(latitude,longitude)
);

CREATE TABLE port(
    latitude NUMERIC(8,6),
    longitude NUMERIC(9,6),
    PRIMARY KEY (latitude, longitude),
    FOREIGN KEY (latitude, longitude) 
        REFERENCES "location"(latitude,longitude)
);

CREATE TABLE schedule(
    "start_date" DATE,
    "end_date" DATE,
    PRIMARY KEY("start_date", "end_date"),
    CHECK ("start_date" < "end_date")
    -- Every schedule must exist
    -- in the table "reservation"
);

CREATE TABLE reservation(
    sched_start_date DATE,
    sched_end_date DATE,
    sailor_id_card VARCHAR(18),
    sailor_iso_code VARCHAR(3),
    boat_cni VARCHAR(14),
    boat_iso_code VARCHAR(3),
    PRIMARY KEY (sched_start_date,sched_end_date,
        sailor_id_card,sailor_iso_code,boat_cni,
        boat_iso_code),
    FOREIGN KEY (sailor_id_card,sailor_iso_code) 
        REFERENCES sailor(id_card,iso_code),
    FOREIGN KEY (boat_iso_code,boat_cni)
        REFERENCES boat(iso_code,cni),
    FOREIGN KEY (sched_start_date,sched_end_date)
        REFERENCES schedule("start_date","end_date")

    -- Reservation schedules of a boat must not overlap
);

CREATE TABLE trip(
    "date" DATE,
    duration INTERVAL NOT NULL,
    sched_start_date DATE,
    sched_end_date DATE,
    sailor_id_card VARCHAR(18),
    sailor_iso_code VARCHAR(3),
    boat_cni VARCHAR(14),
    boat_iso_code VARCHAR(3),
    location_to_latitude NUMERIC(8,6) NOT NULL,
    location_to_longitude NUMERIC(9,6) NOT NULL,
    location_from_latitude NUMERIC(8,6) NOT NULL,
    location_from_longitude NUMERIC(9,6) NOT NULL,
    PRIMARY KEY ("date", sched_start_date,sched_end_date,
        sailor_id_card,sailor_iso_code,boat_cni,
        boat_iso_code),
    FOREIGN KEY (sched_start_date,sched_end_date,
        sailor_id_card,sailor_iso_code,boat_cni,
        boat_iso_code)
        REFERENCES reservation(sched_start_date,sched_end_date,
        sailor_id_card,sailor_iso_code,boat_cni,
        boat_iso_code),
    FOREIGN KEY (location_to_latitude,location_to_longitude)
        REFERENCES "location"(latitude,longitude),
    FOREIGN KEY (location_from_latitude,location_from_longitude)
        REFERENCES "location"(latitude,longitude)

    -- Trips of a reservation must not overlap
);
